### TODO ###

* Heroku app endpoint/ API service
* GET / return list of jobs
* GET /korea return <country> jobs
* GET /admin return list of account jobs
* POST /admin/new add new job to db

### app client features
* job listing sorted by date
* accounts page
* payment page for posting jobs
* jobs input form
* seoul subway map with clickable stations
* links to user driven content based on station
* icons for food, hotel, entertainment, geo-coordinates from station
* hero image is the map/route
* google maps image API (deprecated)
* live search image API

### To run ###

* execute heroku dyno 

### Dependencies ###

* Heroku CLI
* create-react-app CLI
* Hapi/Nodejs
* CouchDB/MongoDB
* React
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)