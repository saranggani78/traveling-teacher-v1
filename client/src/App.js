import React, { Component } from 'react';
import { Router,
  Route,
  Link,
   IndexRoute, // default route
   //hashHistory,
   browserHistory
  } from 'react-router'

import logo from './logo.svg';
import './App.css';


class PageA extends Component{
  render(){
    return(
      <div>Page A content</div>
    )
  }
}

class PageB extends Component{
  render(){
    return(
      <div>Page B content</div>
    )
  }
}

class PageC extends Component{
  render(){
    return(
      <div>Page C content</div>
    )
  }
}
// container for routable pages
class SimplePage extends Component{

  render(){
    const containerStyle = {
      backgroundColor : '#FFCC00',
      // padding: '20px',
      margin:0,
      fontFamily: 'Helvetica, Arial, sans-serif'
    }

    const headerStyle = {
      // display: 'inline-block',
      margin:0,
      // backgroundColor:'#111',
      padding:0,
    }

    const listItemStyle = {

      color:'#FFF',
      // padding: '20px',
      // display: 'inline-block'
    }

    return(
      <div style={containerStyle}>
        <div style={headerStyle}>
          <ul>
            <li><Link to="/taba" style={listItemStyle}>goto Tab A</Link></li>
            <li><Link to="/tabb" style={listItemStyle}>goto Tab B</Link></li>
            <li><Link to="/tabc" style={listItemStyle}>goto Tab C</Link></li>
          </ul>
          <div className="content">
            {this.props.children}
          </div>
        </div>
      </div>)
  }
}

class RouterExample extends Component{

    render(){
    return(
      <Router history={browserHistory}>
        <Route path="/" component={SimplePage}>
          <IndexRoute component={PageA}/>
          <Route path="/taba" component={PageA}/>
          <Route path="/tabb" component={PageB}/>
          <Route path="/tabc" component={PageC}/>
        </Route>
      </Router>
    )
  }
}

// events example
var DisplayCounter = React.createClass({
  render(){
      return(
        <div>{this.props.display}</div>
      )
  }
})

// button declares the event and matches it to handler
// handler is delegated/declared by parent using props
var PlusButton = React.createClass({
  render(){
    return(
      <button onClick={this.props.handleClick}>Custom Add</button>
    );
  }
})

var EventsExample = React.createClass({

  getInitialState(){
    return {count: 0};
  },
  increase(e){
    var currentCount = this.state.count
    if(e.shiftKey){
      currentCount += 10
    }
    else{
      currentCount += 1
    }
    this.setState({
      count: currentCount
    })
  },
  render(){
    return(
    <div>
      <DisplayCounter display={this.state.count}/>
      <button onClick={this.increase}>Add</button>
      <PlusButton handleClick={this.increase}/>
          </div>
  )
  }
})


class App extends Component {
    render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React</h2>
        </div>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        {/* counter component example*/}
          <EventsExample />
      <RouterExample />
      </div>
    );
  }
}

export default App;
